/*
 * DNN SL classification test.
 */

#include <iostream>
#include <limits>
#include <sys/time.h>

#include "TTH/CommonClassifier/interface/DNNClassifier.h"

#ifndef EPSILON
#define EPSILON 1.0e-6
#endif

typedef std::vector<TLorentzVector> TLorentzVectors;
typedef std::vector<double> doubles;

const TLorentzVector makeVector(double pt, double eta, double phi, double mass)
{
    TLorentzVector lv;
    lv.SetPtEtaPhiM(pt, eta, phi, mass);
    return lv;
}

int test_DNN_SL_v6(const std::string& v)
{
    std::cout << "test DNN SL v6" << v << std::endl;
    std::cout << "----------------------------------------" << std::endl << std::endl;

    // set precision of numbers in cout
    std::cout.precision(8);

    // setup the dnn classifier
    DNNClassifier_SL dnn("v6" + v);
    dnn.csvCutMeadium = 0.8484;
    dnn.csvCutTight = 0.9535;

    DNNOutput output4;
    DNNOutput output5;
    DNNOutput output6;

    // model 4j
    {
        TLorentzVectors jets = {
            makeVector(120.608509726, -0.120035373, -2.438538551, 12.035016544),
            makeVector(78.401715909, -1.042814016, -1.299083352, 13.337328703),
            makeVector(73.018615977, 2.276488066, 0.434062839, 14.606420545),
            makeVector(46.417799941, -0.266954660, 0.938573599, 8.755451638)
        };
        doubles jetCSVs = { 0.998804330826, 0.263869464397, 0.923008263111, 0.870377600193 };

        TLorentzVector lepton = makeVector(37.793606915, -0.084358282, 1.639167905, 0.000000000);
        TLorentzVector met = makeVector(25.409085315, 0., -1.488416356, 0.);

        std::vector<double> additionalFeatures;
        additionalFeatures.push_back(0.739335354262); // blr
        additionalFeatures.push_back(1.04251691376); // blr_transformed
        additionalFeatures.push_back(0.0); // mem

        // evaluate
        output4 = dnn.evaluate(jets, jetCSVs, lepton, met, additionalFeatures);
    }

    // model 5j
    {
        TLorentzVectors jets = {
            makeVector(107.630706148, 0.057513133, -0.492661089, 13.108250883),
            makeVector(66.093515825, -1.414399147, -2.071135044, 10.363228208),
            makeVector(61.478474389, -1.397016525, 1.973007679, 11.791041589),
            makeVector(50.004204135, 0.615649164, 2.621977568, 5.636481768),
            makeVector(36.225130819, -0.601126075, -0.547788262, 5.128027444)
        };
        doubles jetCSVs = {
            0.936236441135, 0.879761874676, 0.856111586094, 0.547132670879, 0.252959758043
        };

        TLorentzVector lepton = makeVector(38.161840224, 1.048446059, 1.545904875, 0.105776399);
        TLorentzVector met = makeVector(53.260010776, 0.0, 0.202263060, 0.0);

        std::vector<double> additionalFeatures;
        additionalFeatures.push_back(0.889256656527); // blr
        additionalFeatures.push_back(2.08317059331); // blr_transformed
        additionalFeatures.push_back(0.); // mem

        // evaluate
        output5 = dnn.evaluate(jets, jetCSVs, lepton, met, additionalFeatures);
    }

    // model 6j
    TLorentzVectors jets = {
        makeVector(63.169638852, -2.362748861, 2.959926844, 8.094098736),
        makeVector(62.003562525, -2.284852266, 0.830272675, 9.836133515),
        makeVector(48.662669132, -2.232687712, 2.332225800, 4.242609951),
        makeVector(38.108992403, -0.729226708, -0.613271654, 5.511551399),
        makeVector(36.038590530, -1.922593117, 0.399809033, 7.221994716),
        makeVector(34.153885752, -1.864216328, -2.020997286, 6.904384535)
    };
    doubles jetCSVs = {
        0.191428676248,0.991113126278,0.145819813013,0.767452061176,0.952466607094,0.275389224291
    };

    TLorentzVector lepton = makeVector(63.661004359, -2.039656878, -2.838859558, 0.105778061);
    TLorentzVector met = makeVector(43.933298386, 0., -0.283240888, 0.);

    std::vector<double> additionalFeatures;
    additionalFeatures.push_back(0.710754335); // blr
    additionalFeatures.push_back(0.899050468); // blr_transformed
    additionalFeatures.push_back(0.0); // MEM

    // evaluate
    output6 = dnn.evaluate(jets, jetCSVs, lepton, met, additionalFeatures);

    // some output
    std::vector<DNNOutput> outputs = { output4, output5, output6 };
    for (size_t i = 0; i < outputs.size(); ++i)
    {
        std::cout << "evaluation for model " << (i + 4) << "j:" << std::endl;
        std::cout << "0 - ttH:   " << std::fixed << outputs[i].ttH() << std::endl;
        std::cout << "1 - ttbb:  " << std::fixed << outputs[i].ttbb() << std::endl;
        std::cout << "2 - ttb:   " << std::fixed << outputs[i].ttb() << std::endl;
        std::cout << "3 - tt2b:  " << std::fixed << outputs[i].tt2b() << std::endl;
        std::cout << "4 - ttcc:  " << std::fixed << outputs[i].ttcc() << std::endl;
        std::cout << "5 - ttlf:  " << std::fixed << outputs[i].ttlf() << std::endl;
        std::cout << "6 - other: " << std::fixed << outputs[i].other() << std::endl;
        std::cout << "most probable class: " << outputs[i].mostProbableClass() << std::endl;
        std::cout << std::endl;
    }

    //validate outputs
    doubles targetOutputs4;
    doubles targetOutputs5;
    doubles targetOutputs6;
    if (v == "a")
    {
        targetOutputs4 = {0.049433032, 0.156175665, 0.251782984, 0.173866354, 0.255293558, 0.113448408, 0.0};
        targetOutputs5 = {0.069575120, 0.093149981, 0.157629750, 0.067758874, 0.336004093, 0.275882182, 0.0};
        targetOutputs6 = {0.110958177, 0.194997989, 0.176934422, 0.076261086, 0.197499868, 0.243348457, 0.0};
    }
    else if (v == "b")
    {
        targetOutputs4 = {0.052080093, 0.161334718, 0.234728398, 0.189535138, 0.247049245, 0.115272408, 0.0};
        targetOutputs5 = {0.077204482, 0.082582604, 0.129714255, 0.068028183, 0.325735435, 0.316735040, 0.0};
        targetOutputs6 = {0.104803038, 0.186907569, 0.175216004, 0.085058141, 0.190806408, 0.257208838, 0.0};
    }
    else
    {
        throw std::runtime_error("unknown version: v6" + v);
    }

    for (size_t i = 0; i < targetOutputs4.size(); ++i)
    {
        assert(fabs(targetOutputs4[i] - output4.values[i]) < EPSILON && "The DNN output for 4 jet events is incorrect");
    }
    for (size_t i = 0; i < targetOutputs5.size(); ++i)
    {
        assert(fabs(targetOutputs5[i] - output5.values[i]) < EPSILON && "The DNN output for 5 jet events is incorrect");
    }
    for (size_t i = 0; i < targetOutputs6.size(); ++i)
    {
        assert(fabs(targetOutputs6[i] - output6.values[i]) < EPSILON && "The DNN output for 6 jet events is incorrect");
    }

    return 0;
}

int main()
{
    DNNClassifierBase::pyInitialize();
    test_DNN_SL_v6("a");
    test_DNN_SL_v6("b");
    DNNClassifierBase::pyFinalize();
    return 0;
}

#undef EPSILON
