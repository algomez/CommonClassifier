/*
 * DNN DL classification test.
 */

#include <iostream>
#include <limits>
#include <sys/time.h>

#include "TTH/CommonClassifier/interface/DNNClassifier.h"

#ifndef EPSILON
#define EPSILON 1.0e-6
#endif

typedef std::vector<TLorentzVector> TLorentzVectors;
typedef std::vector<double> doubles;

const TLorentzVector makeVector(double pt, double eta, double phi, double mass)
{
    TLorentzVector lv;
    lv.SetPtEtaPhiM(pt, eta, phi, mass);
    return lv;
}

int test_DNN_DL_v3a()
{
    std::cout << "test DNN DL v3a" << std::endl;
    std::cout << "----------------------------------------" << std::endl << std::endl;

    // set precision of numbers in cout
    std::cout.precision(8);

    // setup the dnn classifier
    DNNClassifier_DL dnn("v3a");
    dnn.csvCutMeadium = 0.8484;
    dnn.csvCutTight = 0.9535;

    DNNOutput output3;
    DNNOutput output4;

    // model 3j
    {
        TLorentzVectors jets = {
            makeVector(78.328530187, 0.144188032, -1.761642098, 10.792411358),
            makeVector(69.388517013, 2.011328459, 1.492962241, 9.922342339),
            makeVector(40.391793495, 0.280981541, 1.834559083, 7.390251887)
        };
        doubles jetCSVs = { 0.920402288437, 0.99479418993, 0.862695157528 };

        TLorentzVectors leptons = {
            makeVector(74.875885407, 1.796993256, -2.524167061, 0.000000000),
            makeVector(29.298543327, -0.414671421, -1.322937965, 0.000000000)
        };
        TLorentzVector met = makeVector(0.0, 0.0, 0.0, 0.0); // not used in this model

        std::vector<double> additionalFeatures;
        additionalFeatures.push_back(0.908149634563); // blr
        additionalFeatures.push_back(2.29124837051); // blr_transformed
        additionalFeatures.push_back(0.); // mem, not used in this model

        // evaluate
        output3 = dnn.evaluate(jets, jetCSVs, leptons, met, additionalFeatures);
    }

    // model 4j
    TLorentzVectors jets = {
        makeVector(117.465839172, 2.116622448, -2.919854641, 11.962203390),
        makeVector(88.776337240, 0.319748104, -1.477689743, 9.565254907),
        makeVector(84.885638574, 1.113329291, 1.052449942, 9.782439626),
        makeVector(22.174411198, -0.171070859, 0.888428628, 4.260208712)
    };
    doubles jetCSVs = { 0.91067302227,0.900372505188,0.982371032238,0.138116180897 };

    TLorentzVectors leptons = {
        makeVector(34.092967072, 1.938921094, 1.009514689, 0.105799295),
        makeVector(27.748626391, 1.056972504, 2.452600002, 0.106044602)
    };
    TLorentzVector met = makeVector(0.0, 0.0, 0.0, 0.0); // not used in this model

    std::vector<double> additionalFeatures;
    additionalFeatures.push_back(0.634909892871); // blr
    additionalFeatures.push_back(0.553338895871); // blr_transformed
    additionalFeatures.push_back(0.); // mem

    // evaluate
    output4 = dnn.evaluate(jets, jetCSVs, leptons, met, additionalFeatures);

    // some output
    std::vector<DNNOutput> outputs = { output3, output4 };
    for (size_t i = 0; i < outputs.size(); ++i)
    {
        std::cout << "evaluation for model " << (i + 3) << "j:" << std::endl;
        std::cout << "0 - ttH:   " << std::fixed << outputs[i].ttH() << std::endl;
        std::cout << "1 - ttbb:  " << std::fixed << outputs[i].ttbb() << std::endl;
        std::cout << "2 - ttb:   " << std::fixed << outputs[i].ttb() << std::endl;
        std::cout << "3 - tt2b:  " << std::fixed << outputs[i].tt2b() << std::endl;
        std::cout << "4 - ttcc:  " << std::fixed << outputs[i].ttcc() << std::endl;
        std::cout << "5 - ttlf:  " << std::fixed << outputs[i].ttlf() << std::endl;
        std::cout << "6 - other: " << std::fixed << outputs[i].other() << std::endl;
        std::cout << "most probable class: " << outputs[i].mostProbableClass() << std::endl;
        std::cout << std::endl;
    }

    // validate outputs
    doubles targetOutputs3 = { 0.100403063, 0.092321970, 0.112557270, 0.090952988, 0.260447776, 0.343316934, 0.0};
    doubles targetOutputs4 = { 0.024452997, 0.058799934, 0.226796717, 0.104884374, 0.266753453, 0.318312526, 0.0};

    for (size_t i = 0; i < targetOutputs3.size(); ++i)
    {
        assert(fabs(targetOutputs3[i] - output3.values[i]) < EPSILON && "The DNN output for 3j events is incorrect");
    }
    for (size_t i = 0; i < targetOutputs4.size(); ++i)
    {
        assert(fabs(targetOutputs4[i] - output4.values[i]) < EPSILON && "The DNN output for 4j events is incorrect");
    }

    return 0;
}

int main()
{
    DNNClassifierBase::pyInitialize();
    test_DNN_DL_v3a();
    DNNClassifierBase::pyFinalize();
    return 0;
}

#undef EPSILON
