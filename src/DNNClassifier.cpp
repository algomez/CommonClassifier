/*
 * DNN Classifier.
 * Please note that this classifier actually outputs 7 discriminator values simultaneously.They can
 * be interpreted as a classification probability as they sum up to 1. Classes (order is important):
 * ttH, ttbb, ttb, tt2b, ttcc, ttlf, other
 */

#include <iostream>

#include "TTH/CommonClassifier/interface/DNNClassifier.h"

void DNNOutput::reset()
{
    for (size_t i = 0; i < 7; i++)
    {
        values[i] = -2.;
    }
}

DNNClassifierBase::DNNClassifierBase(std::string version)
    : csvCutMeadium(0.8484)
    , csvCutTight(0.9535)
    , version_(version)
    , inputName_("inp")
    , outputName_("outp")
    , dropoutName_("keep_prob")
    , pyContext_(0)
    , pyEval_(0)
{
}

DNNClassifierBase::~DNNClassifierBase()
{
    // cleanup python objects
    if (pyEval_) Py_DECREF(pyEval_);
    if (pyContext_) Py_DECREF(pyContext_);
}

void DNNClassifierBase::pyInitialize()
{
    PyEval_InitThreads();
    Py_Initialize();
}

void DNNClassifierBase::pyFinalize()
{
    Py_Finalize();
}

void DNNClassifierBase::pyExcept(PyObject* pyObj, const std::string& msg)
{
    if (pyObj == NULL)
    {
        if (PyErr_Occurred() != NULL)
        {
            PyErr_PrintEx(0);
        }
        throw runtime_error("a python error occured: " + msg);
    }
}

DNNClassifier_SL::DNNClassifier_SL(std::string version)
    : DNNClassifierBase(version)
    , nFeatures4_(0)
    , nFeatures5_(0)
    , nFeatures6_(0)
    , pyEvalArgs4_(0)
    , pyEvalArgs5_(0)
    , pyEvalArgs6_(0)
{
    if (version_ == "v6a") // without MEM
    {
        nFeatures4_ = 35;
        nFeatures5_ = 43;
        nFeatures6_ = 42;
    }
    else if (version_ == "v6b") // with MEM
    {
        nFeatures4_ = 36;
        nFeatures5_ = 44;
        nFeatures6_ = 43;
    }
    else
    {
        throw std::runtime_error("unknown version: " + version_);
    }

    // determine some local paths
    std::string cmsswBase = std::string(getenv("CMSSW_BASE"));
    std::string tfdeployBase = cmsswBase + "/python/TTH/CommonClassifier";
    std::string modelsBase = cmsswBase + "/src/TTH/CommonClassifier/data/dnnmodels_SL_" + version_;
    std::string modelFile4 = modelsBase + "/model_4j.pkl";
    std::string modelFile5 = modelsBase + "/model_5j.pkl";
    std::string modelFile6 = modelsBase + "/model_6j.pkl";

    // initialize the python main object, load the script
    PyObject* pyMainModule = PyImport_AddModule("__main__");

    PyObject* pyMainDict = PyModule_GetDict(pyMainModule);
    pyContext_ = PyDict_Copy(pyMainDict);

    PyRun_String(evalScript.c_str(), Py_file_input, pyContext_, pyContext_);

    // load the tfdeploy models
    PyObject* pySetup = PyDict_GetItemString(pyContext_, "setup");
    PyObject* pyModelFiles = PyTuple_New(3);
    PyTuple_SetItem(pyModelFiles, 0, PyString_FromString(modelFile4.c_str()));
    PyTuple_SetItem(pyModelFiles, 1, PyString_FromString(modelFile5.c_str()));
    PyTuple_SetItem(pyModelFiles, 2, PyString_FromString(modelFile6.c_str()));
    PyObject* pyArgs = PyTuple_New(5);
    PyTuple_SetItem(pyArgs, 0, PyString_FromString(tfdeployBase.c_str()));
    PyTuple_SetItem(pyArgs, 1, pyModelFiles);
    PyTuple_SetItem(pyArgs, 2, PyString_FromString(inputName_.c_str()));
    PyTuple_SetItem(pyArgs, 3, PyString_FromString(outputName_.c_str()));
    PyTuple_SetItem(pyArgs, 4, PyString_FromString(dropoutName_.c_str()));

    PyObject* pyResult = PyObject_CallObject(pySetup, pyArgs);
    pyExcept(pyResult, "could not load tfdeploy models");

    // store the evaluation function and prepare args
    // the "+ 1" is due to the model number being the first argument in the eval function
    pyEval_ = PyDict_GetItemString(pyContext_, "eval");
    pyEvalArgs4_ = PyTuple_New(nFeatures4_ + 1);
    pyEvalArgs5_ = PyTuple_New(nFeatures5_ + 1);
    pyEvalArgs6_ = PyTuple_New(nFeatures6_ + 1);
}

DNNClassifier_SL::~DNNClassifier_SL()
{
    // cleanup python objects
    if (pyEvalArgs4_) Py_DECREF(pyEvalArgs4_);
    if (pyEvalArgs5_) Py_DECREF(pyEvalArgs5_);
    if (pyEvalArgs6_) Py_DECREF(pyEvalArgs6_);
}

void DNNClassifier_SL::evaluate(const std::vector<TLorentzVector>& jets, const doubles& jetCSVs,
    const TLorentzVector& lepton, const TLorentzVector& met, const doubles& additionalFeatures,
    DNNOutput& dnnOutput)
{
    dnnOutput.reset();

    size_t nJets = jets.size();
    size_t modelNum;
    PyObject* pyEvalArgs = NULL;
    if (nJets < 4)
    {
        // no DNN classifier existing for < 4 jets
        return;
    }
    else if (nJets == 4)
    {
        pyEvalArgs = pyEvalArgs4_;
        modelNum = 0;
    }
    else if (nJets == 5)
    {
        pyEvalArgs = pyEvalArgs5_;
        modelNum = 1;
    }
    else
    {
        pyEvalArgs = pyEvalArgs6_;
        modelNum = 2;
    }

    // modelNum is at pos 0
    PyTuple_SetItem(pyEvalArgs, 0, PyInt_FromSize_t(modelNum));

    // fill features into the py tuple
    fillFeatures_(pyEvalArgs, jets, jetCSVs, lepton, met, additionalFeatures);

    // evaluate
    PyObject* pyList = PyObject_CallObject(pyEval_, pyEvalArgs);
    pyExcept(pyList, "could not evaluate models");

    // fill the dnnOutput
    dnnOutput.values.resize(7);
    // no "other" category right now
    bool hasOther = false;
    for (size_t i = 0; i < (hasOther ? 7 : 6); i++)
    {
        dnnOutput.values[i] = PyFloat_AsDouble(PyList_GetItem(pyList, i));
    }
    if (!hasOther)
    {
        dnnOutput.values[6] = 0.0;
    }

    Py_DECREF(pyList);
}

DNNOutput DNNClassifier_SL::evaluate(const std::vector<TLorentzVector>& jets, const doubles& jetCSVs,
const TLorentzVector& lepton, const TLorentzVector& met, const doubles& additionalFeatures)
{
    DNNOutput dnnOutput;
    evaluate(jets, jetCSVs, lepton, met, additionalFeatures, dnnOutput);
    return dnnOutput;
}

void DNNClassifier_SL::fillFeatures_(PyObject* pyEvalArgs, const std::vector<TLorentzVector>& jets,
    const doubles& jetCSVs, const TLorentzVector& lepton, const TLorentzVector& met,
    const doubles& additionalFeatures)
{
    // split jets into bjets, tight bjets and ljets
    // tight bjets are also bjets
    // store pointers to avoid performance drawbacks due to vector copying
    std::vector<const TLorentzVector*> allJets;
    std::vector<const TLorentzVector*> bJets;
    doubles bCSVs;
    std::vector<const TLorentzVector*> tbJets;
    doubles tbCSVs;
    std::vector<const TLorentzVector*> lJets;
    doubles lCSVs;
    size_t nJets = jets.size();
    for (size_t i = 0; i < nJets; i++)
    {
        allJets.push_back(&jets[i]);
        if (jetCSVs[i] >= csvCutTight)
        {
            tbJets.push_back(&jets[i]);
            tbCSVs.push_back(jetCSVs[i]);
        }
        if (jetCSVs[i] >= csvCutMeadium)
        {
            bJets.push_back(&jets[i]);
            bCSVs.push_back(jetCSVs[i]);
        }
        else
        {
            lJets.push_back(&jets[i]);
            lCSVs.push_back(jetCSVs[i]);
        }
    }

    // precompute some variables
    double bAvgDEta, bAvgDR, bAvgCSV, bDev2CSV, bClosestPt, bClosestMass, bClosestMass125;
    dnnVars_.getJetVars(bJets, bCSVs, bAvgDEta, bAvgDR, bAvgCSV, bDev2CSV, bClosestPt, bClosestMass, bClosestMass125);
    double bAvgMass, bAvgMass2;
    dnnVars_.getMassAverages(bJets, bAvgMass, bAvgMass2);
    double jAvgMass, jAvgMass2;
    dnnVars_.getMassAverages(allJets, jAvgMass, jAvgMass2);

    // start filling into pyEvalArgs
    size_t idx = 1;
    if (nJets == 4)
    {
        // jet features
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[0].Pt()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[0].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[0].Phi()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[0]));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[1].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[1].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[1].Phi()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[1]));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[2].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[2].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[2].Phi()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[2]));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[3].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[3].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[3].Phi()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[3]));

        // lepton features
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(lepton.Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(lepton.Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(lepton.Phi()));

        // sumPt
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getHt(allJets)));

        // min/max dR
        double minDR, maxDR;
        // jets
        dnnVars_.getMinMaxDR(allJets, minDR, maxDR);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDR));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDR));

        // aplanarity, centrality, sphericity and transverse sphericity
        double ev1, ev2, ev3, tSphericity;
        dnnVars_.getSphericalEigenValues(allJets, ev1, ev2, ev3);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * ev3)); // aplanarity
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getCentrality(allJets)));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * (ev2 + ev3))); // sphericity
        tSphericity = ev2 == 0 ? 0 : (2. * ev2 / (ev1 + ev2));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(tSphericity));

        // csv max, min and mean
        double maxCSV = -1, minCSV = -1, meanCSV = -1;
        dnnVars_.getCSVStats(jetCSVs, maxCSV, minCSV, meanCSV);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(meanCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minCSV));

        double minDRJetsLep, maxDRJetsLep;
        // jets
        dnnVars_.getMinMaxDR(allJets, &lepton, minDRJetsLep, maxDRJetsLep);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRJetsLep));

        // b-jets
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getHt(bJets)));

        // min/max dR
        dnnVars_.getMinMaxDR(bJets, minDR, maxDR);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDR));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDR));

        // aplanarity, centrality, sphericity and transverse sphericity
        dnnVars_.getSphericalEigenValues(bJets, ev1, ev2, ev3);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * ev3)); // aplanarity
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getCentrality(bJets)));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * (ev2 + ev3))); // sphericity
        tSphericity = ev2 == 0 ? 0 : (2. * ev2 / (ev1 + ev2));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(tSphericity));

        // csv max, min and mean
        maxCSV = -1, minCSV = -1, meanCSV = -1;
        dnnVars_.getCSVStats(bCSVs, maxCSV, minCSV, meanCSV);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(meanCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minCSV));

        // dnnVars_.getMinMaxDR(bJets, &lepton, minDRJetsLep, maxDRJetsLep);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRJetsLep));

        // ljets
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getHt(lJets)));

        // min/max dR
        // dnnVars_.getMinMaxDR(lJets, minDR, maxDR);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDR));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDR));

        // aplanarity, centrality, sphericity and transverse sphericity
        // dnnVars_.getSphericalEigenValues(lJets, ev1, ev2, ev3);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * ev3)); // aplanarity
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getCentrality(lJets)));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * (ev2 + ev3))); // sphericity
        // tSphericity = ev2 == 0 ? 0 : (2. * ev2 / (ev1 + ev2));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(tSphericity));

        // csv max, min and mean
        // maxCSV = -1, minCSV = -1, meanCSV = -1;
        // dnnVars_.getCSVStats(lCSVs, maxCSV, minCSV, meanCSV);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(meanCSV));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxCSV));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minCSV));

        // dnnVars_.getMinMaxDR(lJets, &lepton, minDRJetsLep, maxDRJetsLep);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRJetsLep));

        // fox-wolfram
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.foxWolframN(2, allJets)));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.foxWolframN(3, allJets)));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.foxWolframN(4, allJets)));

        // mass of leading lepton and closest bjet
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getMassLepClosestJet(&lepton, bJets)));

        // variables of jet and bjet pairs
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bAvgDEta));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bAvgDR));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bClosestPt));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bDev2CSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jAvgMass));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble((double)tbJets.size()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bAvgMass2));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bClosestMass125));

        // second highest CSV
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getSecondHighestValue(jetCSVs)));

        // blr, blr_transformed and MEM as additionalFeatures
        if (additionalFeatures.size() != 3)
        {
            throw std::runtime_error("expected 3 additionalFeatures for 4 jets (SL)");
        }
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(additionalFeatures[0])); // blr
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(additionalFeatures[1])); // blr_transformed
        if (version_ == "v6b") {
            // MEM
            PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(std::fmod(std::max(0., additionalFeatures[2]), 1.)));
        }
    }
    else if (nJets == 5)
    {
        // jet features
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[0].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[0].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[0].Phi()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[0]));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[1].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[1].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[1].Phi()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[1]));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[2].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[2].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[2].Phi()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[2]));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[3].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[3].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[3].Phi()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[3]));

        // lepton features
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(lepton.Pt()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(lepton.Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(lepton.Phi()));

        // jets
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getHt(allJets)));
        double minDR, maxDR;
        // min/max dR
        dnnVars_.getMinMaxDR(allJets, minDR, maxDR);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDR));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDR));

        double ev1, ev2, ev3, tSphericity;
        // aplanarity, centrality, sphericity and transverse sphericity
        dnnVars_.getSphericalEigenValues(allJets, ev1, ev2, ev3);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * ev3)); // aplanarity

        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getCentrality(allJets)));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * (ev2 + ev3))); // sphericity
        tSphericity = ev2 == 0 ? 0 : (2. * ev2 / (ev1 + ev2));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(tSphericity));

        // csv max, min and mean
        double maxCSV = -1, minCSV = -1, meanCSV = -1;
        dnnVars_.getCSVStats(jetCSVs, maxCSV, minCSV, meanCSV);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(meanCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minCSV));

        double minDRJetsLep, maxDRJetsLep;
        // min, max dR between jets and leptons
        dnnVars_.getMinMaxDR(allJets, &lepton, minDRJetsLep, maxDRJetsLep);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRJetsLep));

        // bjets
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getHt(bJets)));

        // min/max dR
        dnnVars_.getMinMaxDR(bJets, minDR, maxDR);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDR));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDR));

        // aplanarity, centrality, sphericity and transverse sphericity
        dnnVars_.getSphericalEigenValues(bJets, ev1, ev2, ev3);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * ev3)); // aplanarity
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getCentrality(bJets)));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * (ev2 + ev3))); // sphericity

        tSphericity = ev2 == 0 ? 0 : (2. * ev2 / (ev1 + ev2));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(tSphericity));

        // csv max, min and mean
        maxCSV = -1, minCSV = -1, meanCSV = -1;
        dnnVars_.getCSVStats(bCSVs, maxCSV, minCSV, meanCSV);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(meanCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minCSV));

        // min, max dR between jets and leptons
        dnnVars_.getMinMaxDR(bJets, &lepton, minDRJetsLep, maxDRJetsLep);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRJetsLep));

        // ljets
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getHt(lJets)));

        // min/max dR
        // dnnVars_.getMinMaxDR(lJets, minDR, maxDR);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDR));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDR));

        // aplanarity, centrality, sphericity and transverse sphericity
        // dnnVars_.getSphericalEigenValues(lJets, ev1, ev2, ev3);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * ev3)); // aplanarity
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getCentrality(lJets)));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * (ev2 + ev3))); // sphericity
        // tSphericity = ev2 == 0 ? 0 : (2. * ev2 / (ev1 + ev2));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(tSphericity));

        // csv max, min and mean
        // maxCSV = -1, minCSV = -1, meanCSV = -1;
        // dnnVars_.getCSVStats(lCSVs, maxCSV, minCSV, meanCSV);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(meanCSV));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxCSV));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minCSV));

        // min, max dR between jets and leptons
        // dnnVars_.getMinMaxDR(lJets, &lepton, minDRJetsLep, maxDRJetsLep);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRJetsLep));

        // fox-wolfram
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.foxWolframN(2, allJets)));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.foxWolframN(3, allJets)));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.foxWolframN(4, allJets)));

        // mass of leading lepton and closest bjet
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getMassLepClosestJet(&lepton, bJets)));

        // variables of jet and bjet pairs
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bAvgDEta));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bAvgDR));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bClosestPt));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bDev2CSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jAvgMass));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble((double)tbJets.size()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bAvgMass2));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bClosestMass125));

        // second highest CSV
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getSecondHighestValue(jetCSVs)));

        // blr, blr_transformed and MEM as additionalFeatures
        if (additionalFeatures.size() != 3)
        {
            throw std::runtime_error("expected 3 additionalFeatures for 5 jets (SL)");
        }
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(additionalFeatures[0])); // blr
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(additionalFeatures[1])); // blr_transformed
        if (version_ == "v6b") {
            // MEM
            PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(std::fmod(std::max(0., additionalFeatures[2]), 1.)));
        }
    }
    else
    {
        // jet features
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[0].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[0].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[0].Phi()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[0]));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[1].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[1].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[1].Phi()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[1]));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[2].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[2].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[2].Phi()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[2]));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[3].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[3].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[3].Phi()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[3]));

        // lepton features
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(lepton.Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(lepton.Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(lepton.Phi()));

        // jets
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getHt(allJets)));

        // min/max dR
        double minDR, maxDR;
        dnnVars_.getMinMaxDR(allJets, minDR, maxDR);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDR));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDR));

        double ev1, ev2, ev3, tSphericity;
        // aplanarity, centrality, sphericity and transverse sphericity
        dnnVars_.getSphericalEigenValues(allJets, ev1, ev2, ev3);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * ev3)); // aplanarity
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getCentrality(allJets)));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * (ev2 + ev3))); // sphericity
        tSphericity = ev2 == 0 ? 0 : (2. * ev2 / (ev1 + ev2));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(tSphericity));

        // csv max, min and mean
        double maxCSV = -1, minCSV = -1, meanCSV = -1;
        dnnVars_.getCSVStats(jetCSVs, maxCSV, minCSV, meanCSV);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(meanCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minCSV));

        double minDRJetsLep, maxDRJetsLep;
        // min, max dR between jets and leptons
        // dnnVars_.getMinMaxDR(allJets, &lepton, minDRJetsLep, maxDRJetsLep);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRJetsLep));

        // bjets
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getHt(bJets)));

        // min/max dR
        dnnVars_.getMinMaxDR(bJets, minDR, maxDR);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDR));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDR));

        // aplanarity, centrality, sphericity and transverse sphericity
        dnnVars_.getSphericalEigenValues(bJets, ev1, ev2, ev3);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * ev3)); // aplanarity
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getCentrality(bJets)));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * (ev2 + ev3))); // sphericity
        tSphericity = ev2 == 0 ? 0 : (2. * ev2 / (ev1 + ev2));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(tSphericity));

        // csv max, min and mean
        maxCSV = -1, minCSV = -1, meanCSV = -1;
        dnnVars_.getCSVStats(bCSVs, maxCSV, minCSV, meanCSV);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(meanCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minCSV));

        // min, max dR between jets and leptons
        dnnVars_.getMinMaxDR(bJets, &lepton, minDRJetsLep, maxDRJetsLep);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRJetsLep));

        // ljets
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getHt(lJets)));

        // min/max dR
        // dnnVars_.getMinMaxDR(lJets, minDR, maxDR);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDR));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDR));

        // aplanarity, centrality, sphericity and transverse sphericity
        // dnnVars_.getSphericalEigenValues(lJets, ev1, ev2, ev3);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * ev3)); // aplanarity
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getCentrality(lJets)));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * (ev2 + ev3))); // sphericity
        // tSphericity = ev2 == 0 ? 0 : (2. * ev2 / (ev1 + ev2));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(tSphericity));

        // csv max, min and mean
        // maxCSV = -1, minCSV = -1, meanCSV = -1;
        // dnnVars_.getCSVStats(lCSVs, maxCSV, minCSV, meanCSV);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(meanCSV));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxCSV));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minCSV));

        // min, max dR between jets and leptons
        // dnnVars_.getMinMaxDR(lJets, &lepton, minDRJetsLep, maxDRJetsLep);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRJetsLep));

        // fox-wolfram
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.foxWolframN(2, allJets)));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.foxWolframN(3, allJets)));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.foxWolframN(4, allJets)));

        // mass of leading lepton and closest bjet
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getMassLepClosestJet(&lepton, bJets)));

        // variables of jet and bjet pairs
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bAvgDEta));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bAvgDR));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bClosestPt));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bDev2CSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jAvgMass));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble((double)tbJets.size()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bAvgMass2));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bClosestMass125));

        // second highest CSV
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getSecondHighestValue(jetCSVs)));

        // blr, blr_transformed and MEM as additionalFeatures
        if (additionalFeatures.size() != 3)
        {
            throw std::runtime_error("expected 3 additionalFeatures for 6 jets (SL)");
        }
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(additionalFeatures[0])); // blr
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(additionalFeatures[1])); // blr_transformed
        if (version_ == "v6b") {
            // MEM
            PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(std::fmod(std::max(0., additionalFeatures[2]), 1.)));
        }
    }
}

DNNClassifier_DL::DNNClassifier_DL(std::string version)
    : DNNClassifierBase(version)
    , nFeatures3_(0)
    , nFeatures4_(0)
    , pyEvalArgs3_(0)
    , pyEvalArgs4_(0)
{
    // set feature numbers based in the version
    if (version_ == "v3a") // TODO
    {
        nFeatures3_ = 34;
        nFeatures4_ = 40;
    }
    else
    {
        throw std::runtime_error("unknown version: " + version_);
    }

    // determine some local paths
    std::string cmsswBase = std::string(getenv("CMSSW_BASE"));
    std::string tfdeployBase = cmsswBase + "/python/TTH/CommonClassifier";
    std::string modelsBase = cmsswBase + "/src/TTH/CommonClassifier/data/dnnmodels_DL_" + version_;
    std::string modelFile3 = modelsBase + "/model_3j.pkl";
    std::string modelFile4 = modelsBase + "/model_4j.pkl";

    // initialize the python main object, load the script
    PyObject* pyMainModule = PyImport_AddModule("__main__");

    PyObject* pyMainDict = PyModule_GetDict(pyMainModule);
    pyContext_ = PyDict_Copy(pyMainDict);

    PyRun_String(evalScript.c_str(), Py_file_input, pyContext_, pyContext_);

    // load the tfdeploy models
    PyObject* pySetup = PyDict_GetItemString(pyContext_, "setup");
    PyObject* pyModelFiles = PyTuple_New(2);
    PyTuple_SetItem(pyModelFiles, 0, PyString_FromString(modelFile3.c_str()));
    PyTuple_SetItem(pyModelFiles, 1, PyString_FromString(modelFile4.c_str()));
    PyObject* pyArgs = PyTuple_New(5);
    PyTuple_SetItem(pyArgs, 0, PyString_FromString(tfdeployBase.c_str()));
    PyTuple_SetItem(pyArgs, 1, pyModelFiles);
    PyTuple_SetItem(pyArgs, 2, PyString_FromString(inputName_.c_str()));
    PyTuple_SetItem(pyArgs, 3, PyString_FromString(outputName_.c_str()));
    PyTuple_SetItem(pyArgs, 4, PyString_FromString(dropoutName_.c_str()));

    PyObject* pyResult = PyObject_CallObject(pySetup, pyArgs);
    pyExcept(pyResult, "could not load tfdeploy models");

    // store the evaluation function and prepare args
    // the "+ 1" is due to the model number being the first argument in the eval function
    pyEval_ = PyDict_GetItemString(pyContext_, "eval");
    pyEvalArgs3_ = PyTuple_New(nFeatures3_ + 1);
    pyEvalArgs4_ = PyTuple_New(nFeatures4_ + 1);
}

DNNClassifier_DL::~DNNClassifier_DL()
{
    // cleanup python objects
    if (pyEvalArgs3_) Py_DECREF(pyEvalArgs3_);
    if (pyEvalArgs4_) Py_DECREF(pyEvalArgs4_);
}

void DNNClassifier_DL::evaluate(const std::vector<TLorentzVector>& jets, const doubles& jetCSVs,
    const std::vector<TLorentzVector>& leptons, const TLorentzVector& met,
    const doubles& additionalFeatures, DNNOutput& dnnOutput)
{
    dnnOutput.reset();

    size_t nJets = jets.size();
    size_t modelNum;
    PyObject* pyEvalArgs = NULL;
    if (nJets < 3)
    {
        // no DNN classifier existing for < 3 jets
        return;
    }
    else if (nJets == 3)
    {
        pyEvalArgs = pyEvalArgs3_;
        modelNum = 0;
    }
    else
    {
        pyEvalArgs = pyEvalArgs4_;
        modelNum = 1;
    }

    // modelNum is at pos 0
    PyTuple_SetItem(pyEvalArgs, 0, PyInt_FromSize_t(modelNum));

    // fill features into the py tuple
    fillFeatures_(pyEvalArgs, jets, jetCSVs, leptons, met, additionalFeatures);

    // debug line to print feautures
    // for (size_t i = 0; i <= (modelNum == 0 ? nFeatures3_ : nFeatures4_); i++)
    // {
    //     std::cout << "feature " << i << ": " << PyFloat_AsDouble(PyTuple_GetItem(pyEvalArgs, i)) << std::endl;
    // }

    // evaluate
    PyObject* pyList = PyObject_CallObject(pyEval_, pyEvalArgs);
    pyExcept(pyList, "could not evaluate models");

    // fill the dnnOutput
    dnnOutput.values.resize(7);
    // v1, v2a and v2b do not have the "other" category
    bool hasOther = false;
    for (size_t i = 0; i < (hasOther ? 7 : 6); i++)
    {
        dnnOutput.values[i] = PyFloat_AsDouble(PyList_GetItem(pyList, i));
    }
    if (!hasOther)
    {
        dnnOutput.values[6] = 0.0;
    }

    Py_DECREF(pyList);
}

DNNOutput DNNClassifier_DL::evaluate(const std::vector<TLorentzVector>& jets, const doubles& jetCSVs,
    const std::vector<TLorentzVector>& leptons, const TLorentzVector& met,
    const doubles& additionalFeatures)
{
    DNNOutput dnnOutput;
    evaluate(jets, jetCSVs, leptons, met, additionalFeatures, dnnOutput);
    return dnnOutput;
}

void DNNClassifier_DL::fillFeatures_(PyObject* pyEvalArgs, const std::vector<TLorentzVector>& jets,
    const doubles& jetCSVs, const std::vector<TLorentzVector>& leptons, const TLorentzVector& met,
    const doubles& additionalFeatures)
{
    // split jets into bjets, tight bjets and ljets
    // tight bjets are also bjets
    // store pointers to avoid performance drawbacks due to vector copying
    std::vector<const TLorentzVector*> allJets;
    std::vector<const TLorentzVector*> bJets;
    doubles bCSVs;
    std::vector<const TLorentzVector*> tbJets;
    doubles tbCSVs;
    std::vector<const TLorentzVector*> lJets;
    doubles lCSVs;
    size_t nJets = jets.size();
    for (size_t i = 0; i < nJets; i++)
    {
        allJets.push_back(&jets[i]);
        if (jetCSVs[i] >= csvCutTight)
        {
            tbJets.push_back(&jets[i]);
            tbCSVs.push_back(jetCSVs[i]);
        }
        if (jetCSVs[i] >= csvCutMeadium)
        {
            bJets.push_back(&jets[i]);
            bCSVs.push_back(jetCSVs[i]);
        }
        else
        {
            lJets.push_back(&jets[i]);
            lCSVs.push_back(jetCSVs[i]);
        }
    }

    // precompute some variables
    double bAvgDEta, bAvgDR, bAvgCSV, bDev2CSV, bClosestPt, bClosestMass, bClosestMass125;
    dnnVars_.getJetVars(bJets, bCSVs, bAvgDEta, bAvgDR, bAvgCSV, bDev2CSV, bClosestPt, bClosestMass, bClosestMass125);
    double bAvgMass, bAvgMass2;
    dnnVars_.getMassAverages(bJets, bAvgMass, bAvgMass2);
    double jAvgMass, jAvgMass2;
    dnnVars_.getMassAverages(allJets, jAvgMass, jAvgMass2);

    // start filling into pyEvalArgs
    size_t idx = 1;
    if (nJets == 3)
    {
        // jet features
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[0].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[0].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[0].Phi()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[0]));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[1].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[1].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[1].Phi()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[1]));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[2].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[2].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[2].Phi()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[2]));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[3].Pt()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[3].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[3].Phi()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[3]));

        // lepton features
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(leptons[0].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(leptons[0].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(leptons[0].Phi()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(leptons[1].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(leptons[1].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(leptons[1].Phi()));

        // all jets
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getHt(allJets)));
        // min/max dR
        // dnnVars_.getMinMaxDR(allJets, minDR, maxDR);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDR));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDR));

        // aplanarity, centrality, sphericity and transverse sphericity
        double ev1, ev2, ev3, tSphericity;
        dnnVars_.getSphericalEigenValues(allJets, ev1, ev2, ev3);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * ev3)); // aplanarity
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getCentrality(allJets)));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * (ev2 + ev3))); // sphericity
        // tSphericity = ev2 == 0 ? 0 : (2. * ev2 / (ev1 + ev2));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(tSphericity));

        // csv max, min and mean
        double maxCSV = -1, minCSV = -1, meanCSV = -1;
        dnnVars_.getCSVStats(jetCSVs, maxCSV, minCSV, meanCSV);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(meanCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minCSV));

        // b-jets
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getHt(bJets)));

        // min/max dR
        // double minDR, maxDR;
        // dnnVars_.getMinMaxDR(bJets, minDR, maxDR);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDR));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDR));

        // aplanarity, centrality, sphericity and transverse sphericity
        // dnnVars_.getSphericalEigenValues(bJets, ev1, ev2, ev3);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * ev3)); // aplanarity
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getCentrality(bJets)));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * (ev2 + ev3))); // sphericity
        // tSphericity = ev2 == 0 ? 0 : (2. * ev2 / (ev1 + ev2));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(tSphericity));

        // csv max, min and mean
        // maxCSV = -1, minCSV = -1, meanCSV = -1;
        // dnnVars_.getCSVStats(bCSVs, maxCSV, minCSV, meanCSV);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(meanCSV));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxCSV));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minCSV));

        // ljets
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getHt(lJets)));

        // min/max dR
        // dnnVars_.getMinMaxDR(lJets, minDR, maxDR);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDR));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDR));

        // aplanarity, centrality, sphericity and transverse sphericity
        // dnnVars_.getSphericalEigenValues(lJets, ev1, ev2, ev3);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * ev3)); // aplanarity
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getCentrality(lJets)));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * (ev2 + ev3))); // sphericity
        // tSphericity = ev2 == 0 ? 0 : (2. * ev2 / (ev1 + ev2));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(tSphericity));

        // maxCSV = -1, minCSV = -1, meanCSV = -1;
        // dnnVars_.getCSVStats(lCSVs, maxCSV, minCSV, meanCSV);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(meanCSV));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxCSV));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minCSV));

        // min, max dR between jets and leptons
        // double minDRJetsLep, maxDRJetsLep;
        // // jets
        // dnnVars_.getMinMaxDR(allJets, &lepton, minDRJetsLep, maxDRJetsLep);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRJetsLep));
        // // bjets
        // dnnVars_.getMinMaxDR(bJets, &lepton, minDRJetsLep, maxDRJetsLep);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRJetsLep));
        // // ljets
        // dnnVars_.getMinMaxDR(lJets, &lepton, minDRJetsLep, maxDRJetsLep);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRJetsLep));

        // fox-wolfram
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.foxWolframN(2, allJets)));

        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.foxWolframN(3, allJets)));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.foxWolframN(4, allJets)));

        // mass of leading lepton and closest bjet
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getMassLepClosestJet(&leptons[0], bJets)));

        // variables of jet and bjet pairs
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bAvgDEta));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bAvgDR));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bClosestPt));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bDev2CSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jAvgMass));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble((double)tbJets.size()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bAvgMass2));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bClosestMass125));

        // second highest CSV
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getSecondHighestValue(jetCSVs)));
        // mll
        // double mll = (leptons[0] + leptons[1]).M();
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(mll));

        // blr, blr_transformed and MEM as additionalFeatures
        if (additionalFeatures.size() != 3)
        {
            throw std::runtime_error("expected 3 additionalFeatures for 3 jets (DL)");
        }
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(additionalFeatures[0])); // blr
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(additionalFeatures[1])); // blr_transformed
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(std::fmod(std::max(0., additionalFeatures[2]), 1.))); // MEM
    }
    else
    {
        // jet features
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[0].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[0].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[0].Phi()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[0]));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[1].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[1].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[1].Phi()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[1]));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[2].Pt()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[2].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[2].Phi()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[2]));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[3].Pt()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[3].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[3].Phi()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[3]));

        // lepton features
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(leptons[0].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(leptons[0].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(leptons[0].Phi()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(leptons[1].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(leptons[1].Eta()));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(leptons[1].Phi()));

        // jets

        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getHt(allJets)));

        // min/max dR
        double minDR, maxDR;
        dnnVars_.getMinMaxDR(allJets, minDR, maxDR);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDR));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDR));

        // aplanarity, centrality, sphericity and transverse sphericity
        double ev1, ev2, ev3, tSphericity;
        dnnVars_.getSphericalEigenValues(allJets, ev1, ev2, ev3);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * ev3)); // aplanarity
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getCentrality(allJets)));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * (ev2 + ev3))); // sphericity
        tSphericity = ev2 == 0 ? 0 : (2. * ev2 / (ev1 + ev2));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(tSphericity));

        // csv max, min and mean
        double maxCSV = -1, minCSV = -1, meanCSV = -1;
        dnnVars_.getCSVStats(jetCSVs, maxCSV, minCSV, meanCSV);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(meanCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minCSV));

        // b-jets

        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getHt(bJets)));

        // min/max dR
        dnnVars_.getMinMaxDR(bJets, minDR, maxDR);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDR));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDR));

        // aplanarity, centrality, sphericity and transverse sphericity
        dnnVars_.getSphericalEigenValues(bJets, ev1, ev2, ev3);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * ev3)); // aplanarity
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getCentrality(bJets)));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * (ev2 + ev3))); // sphericity
        tSphericity = ev2 == 0 ? 0 : (2. * ev2 / (ev1 + ev2));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(tSphericity));

        // csv max, min and mean
        maxCSV = -1, minCSV = -1, meanCSV = -1;
        dnnVars_.getCSVStats(bCSVs, maxCSV, minCSV, meanCSV);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(meanCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxCSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minCSV));

        // ljets

        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getHt(lJets)));

        // dnnVars_.getMinMaxDR(lJets, minDR, maxDR);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDR));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDR));

        // ljets
        // dnnVars_.getSphericalEigenValues(lJets, ev1, ev2, ev3);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * ev3)); // aplanarity
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getCentrality(lJets)));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * (ev2 + ev3))); // sphericity
        // tSphericity = ev2 == 0 ? 0 : (2. * ev2 / (ev1 + ev2));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(tSphericity));

        // ljets
        // maxCSV = -1, minCSV = -1, meanCSV = -1;
        // dnnVars_.getCSVStats(lCSVs, maxCSV, minCSV, meanCSV);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(meanCSV));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxCSV));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minCSV));

        // min, max dR between jets and leptons
        // double minDRJetsLep, maxDRJetsLep;
        // // jets
        // dnnVars_.getMinMaxDR(allJets, &lepton, minDRJetsLep, maxDRJetsLep);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRJetsLep));
        // // bjets
        // dnnVars_.getMinMaxDR(bJets, &lepton, minDRJetsLep, maxDRJetsLep);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRJetsLep));
        // // ljets
        // dnnVars_.getMinMaxDR(lJets, &lepton, minDRJetsLep, maxDRJetsLep);
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRJetsLep));

        // fox-wolfram
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.foxWolframN(2, allJets)));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.foxWolframN(3, allJets)));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.foxWolframN(4, allJets)));

        // mass of leading lepton and closest bjet
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getMassLepClosestJet(&leptons[0], bJets)));


        // variables of jet and bjet pairs
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bAvgDEta));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bAvgDR));
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bClosestPt));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bDev2CSV));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jAvgMass));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble((double)tbJets.size()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bAvgMass2));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(bClosestMass125));

        // second highest CSV
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getSecondHighestValue(jetCSVs)));

        // mll
        double mll = (leptons[0] + leptons[1]).M();
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(mll));

        // blr, blr_transformed and MEM as additionalFeatures
        if (additionalFeatures.size() != 3)
        {
            throw std::runtime_error("expected 3 additionalFeatures for 4 jets (DL)");
        }
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(additionalFeatures[0])); // blr
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(additionalFeatures[1])); // blr_transformed
        // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(std::fmod(std::max(0., additionalFeatures[2]), 1.))); // MEM
    }
}

double DNNVariables::getHt(const std::vector<const TLorentzVector*>& lvecs) const
{
    double ht = 0;
    for (size_t i = 0; i < lvecs.size(); i++)
    {
        ht += lvecs[i]->Pt();
    }
    return ht;
}

void DNNVariables::getMinMaxDR(
    const std::vector<const TLorentzVector*>& lvecs, double& minDR, double& maxDR) const
{
    maxDR = lvecs.size() == 0 ? -1 : 0.;
    minDR = lvecs.size() == 0 ? -1 : 100.;
    if (lvecs.size() >= 2)
    {
        for (size_t i = 0; i < lvecs.size() - 1; i++)
        {
            for (size_t j = i + 1; j < lvecs.size(); j++)
            {
                double dR = lvecs[i]->DeltaR(*lvecs[j]);
                if (dR > maxDR)
                {
                    maxDR = dR;
                }
                if (dR < minDR)
                {
                    minDR = dR;
                }
            }
        }
    }
}

void DNNVariables::getMinMaxDR(const std::vector<const TLorentzVector*>& lvecs,
    const TLorentzVector* lvec, double& minDR, double& maxDR) const
{
    maxDR = lvecs.size() == 0 ? -1 : 0.;
    minDR = lvecs.size() == 0 ? -1 : 100.;
    for (size_t i = 0; i < lvecs.size(); i++)
    {
        double dR = lvecs[i]->DeltaR(*lvec);
        if (dR > maxDR)
        {
            maxDR = dR;
        }
        if (dR < minDR)
        {
            minDR = dR;
        }
    }
}

double DNNVariables::getCentrality(const std::vector<const TLorentzVector*>& lvecs) const
{
    double sumPt = 0.;
    double sumP = 0.;
    for (size_t i = 0; i < lvecs.size(); i++)
    {
        sumPt += lvecs[i]->Pt();
        sumP += lvecs[i]->P();
    }
    return sumPt / sumP;
}

void DNNVariables::getSphericalEigenValues(
    const std::vector<const TLorentzVector*>& lvecs, double& ev1, double& ev2, double& ev3) const
{
    TMatrixDSym momentumMatrix(3);
    double p2Sum = 0.;

    for (size_t i = 0; i < lvecs.size(); i++)
    {
        double px = lvecs[i]->Px();
        double py = lvecs[i]->Py();
        double pz = lvecs[i]->Pz();

        // fill the matrix
        momentumMatrix(0, 0) += px * px;
        momentumMatrix(0, 1) += px * py;
        momentumMatrix(0, 2) += px * pz;
        momentumMatrix(1, 0) += py * px;
        momentumMatrix(1, 1) += py * py;
        momentumMatrix(1, 2) += py * pz;
        momentumMatrix(2, 0) += pz * px;
        momentumMatrix(2, 1) += pz * py;
        momentumMatrix(2, 2) += pz * pz;

        // add 3 momentum squared to sum
        p2Sum += px * px + py * py + pz * pz;
    }

    // normalize each element by p2Sum
    if (p2Sum != 0.)
    {
        for (size_t i = 0; i < 3; i++)
        {
            for (size_t j = 0; j < 3; j++)
            {
                momentumMatrix(i, j) = momentumMatrix(i, j) / p2Sum;
            }
        }
    }

    // calculatate eigen values via eigen vectors
    TMatrixDSymEigen eig(momentumMatrix);
    TVectorD ev = eig.GetEigenValues();

    // some checks due to limited precision of TVectorD
    ev1 = fabs(ev[0]) < 0.00000000000001 ? 0 : ev[0];
    ev2 = fabs(ev[1]) < 0.00000000000001 ? 0 : ev[1];
    ev3 = fabs(ev[2]) < 0.00000000000001 ? 0 : ev[2];
}

double DNNVariables::cosTheta(const TLorentzVector* particle1, const TLorentzVector* particle2) const
{
    if (particle1 == particle2)
    {
        return 1.;
    }

    double px1 = particle1->Px();
    double py1 = particle1->Py();
    double pz1 = particle1->Pz();
    double px2 = particle2->Px();
    double py2 = particle2->Py();
    double pz2 = particle2->Pz();

    double cos_angle = double(px1 * px2 + py1 * py2 + pz1 * pz2);
    cos_angle = cos_angle / sqrt(px1 * px1 + py1 * py1 + pz1 * pz1);
    cos_angle = cos_angle / sqrt(px2 * px2 + py2 * py2 + pz2 * pz2);

    return cos_angle;
}

double DNNVariables::legendreN(int n, const TLorentzVector* p1, const TLorentzVector* p2) const
{
    double c = cosTheta(p1, p2);
    double c2 = c * c;

    if (n == 0)
    {
        return 1.;
    }
    else if (n == 1)
    {
        return c;
    }
    else if (n == 2)
    {
        return 0.5 * (3. * c2 - 1.);
    }
    else if (n == 3)
    {
        return 0.5 * (5. * c * c2 - 3. * c);
    }
    else if (n == 4)
    {
        return 0.125 * (35. * c2 * c2 - 30. * c2 + 3.);
    }
    else
    {
        throw std::runtime_error("legendreN not implemented for n > 4");
    }

    return 0.;
}

double DNNVariables::foxWolframN(int n, const std::vector<const TLorentzVector*>& particles) const
{
    double evis = 0.; // visible energy
    double res = 0.; // fox-wolfram-moment

    for (size_t i = 0; i < particles.size(); ++i)
    {
        const TLorentzVector* p1 = particles[i];
        evis += p1->E();
        for (size_t j = 0; j < particles.size(); ++j)
        {
            const TLorentzVector* p2 = particles[j];
            res += p1->P() * p2->P() * legendreN(n, p1, p2);
        }
    }

    res = res / (evis * evis);

    return res;
}

void DNNVariables::getCSVStats(const doubles& jetCSVs, double& maxCSV, double& minCSV,
    double& meanCSV) const
{
    if (jetCSVs.size() == 0)
    {
        return;
    }

    maxCSV = -1e5;
    minCSV = -1e5;
    double sumCSV = 0.;

    for (size_t i = 0; i < jetCSVs.size(); i++)
    {
        if (maxCSV == -1e5 || jetCSVs[i] > maxCSV)
        {
            maxCSV = jetCSVs[i];
        }
        if (minCSV == -1e5 || jetCSVs[i] < minCSV)
        {
            minCSV = jetCSVs[i];
        }
        sumCSV += jetCSVs[i];
    }
    meanCSV = sumCSV / jetCSVs.size();
}

double DNNVariables::getMassLepClosestJet(const TLorentzVector* lep,
    const std::vector<const TLorentzVector*>& lvecs) const
{
    const TLorentzVector* closestJet = nullptr;
    double minDR = 1e5;
    for (size_t i = 0; i < lvecs.size(); i++)
    {
        double dR = lep->DeltaR(*lvecs[i]);
        if (dR < minDR)
        {
            minDR = dR;
            closestJet = lvecs[i];
        }
    }
    return closestJet == nullptr ? -1. : (*lep + *closestJet).M();
}

void DNNVariables::getJetVars(const std::vector<const TLorentzVector*>& lvecs,
    const doubles& jetCSVs, double& avgDEta, double& avgDR, double& avgCSV, double& dev2CSV,
    double& closestPt, double& closestMass, double& closestMass125) const
{
    size_t nVecs = lvecs.size();
    size_t nCombis = (nVecs * nVecs - nVecs) / 2;

    if (nVecs != jetCSVs.size())
    {
        throw std::runtime_error("number of jets does not match number of csv values");
    }
    else if (nVecs == 0)
    {
        avgDEta = -1.;
        avgDR = -1.;
        avgCSV = -1.;
        dev2CSV = -1.;
        closestPt = -1.;
        closestMass = -1.;
        closestMass125 = -1.;
        return;
    }

    double sumDEta = 0.;
    double sumDR = 0.;
    double sumCSV = 0.;
    double minDR = 1e5;
    closestMass125 = 1e5;
    for (size_t i = 0; i < nVecs; i++)
    {
        sumCSV += jetCSVs[i];

        for (size_t j = i + 1; j < nVecs; j++)
        {
            TLorentzVector comb = *lvecs[i] + *lvecs[j];

            double dR = lvecs[i]->DeltaR(*lvecs[j]);
            if (dR < minDR)
            {
                minDR = dR;
                closestPt = comb.Pt();
                closestMass = comb.M();
            }

            sumDEta += abs(lvecs[i]->Eta() - lvecs[j]->Eta());
            sumDR += dR;

            double mass = comb.M();
            if (fabs(125. - mass) < (fabs(closestMass125 - 125)))
            {
                closestMass125 = mass;
            }
        }
    }

    avgDEta = sumDEta / (double)nCombis;
    avgDR = sumDR / (double)nCombis;
    avgCSV = sumCSV / (double)nVecs;
    dev2CSV = 0.;
    for (size_t i = 0; i < nVecs; i++)
    {
        dev2CSV += pow(avgCSV - jetCSVs[i], 2);
    }
    dev2CSV /= (double)nVecs;
}

void DNNVariables::getMassAverages(const std::vector<const TLorentzVector*>& lvecs,
    double& avgMass, double& avgMass2) const
{
    if (lvecs.size() == 0)
    {
        avgMass = -1.;
        avgMass2 = -1.;
        return;
    }

    double sumMass = 0.;
    double sumMass2 = 0.;
    for (size_t i = 0; i < lvecs.size(); i++)
    {
        sumMass += lvecs[i]->M();
        sumMass2 += lvecs[i]->M() * lvecs[i]->M();
    }

    avgMass = sumMass / (double)lvecs.size();
    avgMass2 = sumMass2 / (double)lvecs.size();
}

double DNNVariables::getSecondHighestValue(const doubles& vals) const
{
    if (vals.size() < 2)
    {
        return -1.;
    }

    double first = -1e5;
    double second = -1e5;
    for (size_t i = 0; i < vals.size(); i++)
    {
        double v = vals[i];
        if (v > first)
        {
            second = first;
            first = v;
        }
        else if (v > second)
        {
            second = v;
        }
    }

    return second;
}
